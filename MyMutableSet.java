package mySets;

public class MyMutableSet<T> extends MyAbstractSet<T> implements java.util.Set<MySetElement<T>> {
  public <T> MyMutableSet() {
    super(null);
  }

  public <T> MyMutableSet(T elm) {
    super(elm);
  }

  public void clear() {
    // everything in me screams memory leak... But Java is garbage collected.
    head = null;
  }

  public boolean remove(Object o) {
    if (!this.contains(o)) return false;

    MySetIterator<T> it = iterator();

    // NOTE: Since we know the Element is in this set, we know it has at least one Element.
    MySetElement<T> prev = it.next();

    if (prev.equals(o)) {
      assert(prev.equals(head));
      head = null;
      return true;
    } else {
      while(it.hasNext()) {
        MySetElement<T> next = it.next();

        if (next.equals(o)) {
          prev.next = it.hasNext() ? it.next() : null;
          return true;
        } else {
          prev = next;
        }
      }
    }

    return false;
  }
  public boolean removeAll(java.util.Collection<?> c) {
    return false;
  }

  public boolean add(MySetElement<T> o) {
    MySetIterator<T> it = iterator();
    if (this.contains(o)) return false;

    while(it.hasNext()) {
      it.next();
      if (!it.hasNext()) {
        it.current.next = o;
      }
    }
    return false;
  }
  public boolean addAll(java.util.Collection<? extends MySetElement<T>> c) {
    boolean modified = false;
    for(MySetElement<T> elm : c) {
      if(add(elm)) modified = true;
    }
    return modified;
  }


  public boolean retainAll(java.util.Collection<?> c) {
    throw new java.lang.UnsupportedOperationException();
  }


  public boolean containsAll(java.util.Collection<?> c) {
    for(Object elm : c) {
      if(!this.contains(elm)) return false;
    }
    return true;
  }


  @Override
  public boolean equals (Object other) {
    if (other instanceof MyAbstractSet<?>) {
      return this.containsAll ((java.util.Collection<?>) other)
          && this.size() == ((MyAbstractSet<?>) other).size();
    }
    return false ;
  }

  MyMutableSet<MySetElement<MyMutableSet<T>>> powerset() {
    MySetIterator<T> it = iterator();

    // include the empty set
    var out = new MyMutableSet<MyMutableSet<T>>(new MySetElement<MyMutableSet<T>>(new MyMutableSet<T>(null)));

    for(MySetElement<T> e : this) {
      out.add(new MyMutableSet<T>(e));
    }
  }
}
