package mySets;

class MySetElement<T> {
  private T value;
  public MySetElement<T> next;

  MySetElement(T value) {
    this.value = value;
    this.next = null;
  }
}

class MySetIterator<T> implements java.util.Iterator<MySetElement<T>> {
  MySetElement<T> current;

  MySetIterator(MySetElement<T> current) {
    this.current = current;
  }

  public boolean hasNext() {
    return !current.equals(null);
  }

  public MySetElement<T> next() {
    if (current.equals(null)) throw new java.util.NoSuchElementException();
    return current;
  }

  public void remove(T elm) {
    throw new java.lang.UnsupportedOperationException();
  }
}

abstract class MyAbstractSet<T> implements java.lang.Iterable<MySetElement<T>>, java.util.Set<MySetElement<T>> {
  MySetElement<T> head;

  public <T> MyAbstractSet(T head) {
    this.head = head;
  }

  public MySetIterator<T> iterator() {
    return new MySetIterator<T>(head);
  }

  public boolean contains(Object o) {
    MySetIterator<T> it = iterator();
    while (it.hasNext()) {
      if (it.next().equals(o)) return true;
    }

    return false;
  }

  public boolean isEmpty() {
    return head.equals(null);
  }

  public int size() {
    MySetIterator<T> it = iterator();

    int ret = 0;
    while (it.hasNext()) {
      ret += 1;
    }
    return ret;
  }

  public Object[] toArray() {
    throw new java.lang.UnsupportedOperationException();
  }

  public <T> T[] toArray(T[] a) {
    throw new java.lang.UnsupportedOperationException();
  }

  public String toString() {
    MySetIterator<T> it = new MySetIterator<T>(head);

    String out = "{";
    while (it.hasNext()) {
      out += it.next().toString() + ",";
    }

    out += "}";
    return out;
  }
}
